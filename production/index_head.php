<div class="block_green visible">
    <div class="body visible">
        <h1>Продукция "Premiko"</h1>
        <div class="txt">На протяжении веков люди изобретали и улучшали самые разнообразные способы сохранения и
            консервирования продуктов. Стоит углубиться в тайны основных методов, благодаря которым наша кулинарная
            жизнь стала более качественной, интересной, а в первую очередь свежей.
        </div>
        <a class="sticker_midi" href="tovar.html" style="bottom: -80px;">
            <span>Скачать прайс-лист (pdf.350Kb)</span>
        </a>
    </div><!--/.body -->
    <div class="block_green_down"></div>
</div>