<?php
const PRODUCT_PIC_WIDTH = 122;
const PRODUCT_PIC_HEIGHT = 124;

AddEventHandler("main", "OnEpilog", "Error404");
function Error404() {

    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        CHTTP::SetStatus("404 Not Found");
        $asset = \Bitrix\Main\Page\Asset::getInstance();
        include($_SERVER["DOCUMENT_ROOT"]. "/local/templates/test/header.php");
        include($_SERVER["DOCUMENT_ROOT"] . "/404.php");
        include($_SERVER["DOCUMENT_ROOT"].  "/local/templates/test/footer.php");
        return true;
    }
}