<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
        "SHOW_TITLE"  =>  array(
            "PARENT"    =>  "BASE",
            "NAME"      =>  "Отображать заголовок",
            "TYPE"      =>  "CHECKBOX",
            "DEFAULT"   =>  "Y"
        ),
);