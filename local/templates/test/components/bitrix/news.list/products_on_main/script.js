$(function() {
    $(".carousel").jCarouselLite({
        btnNext: ".prev",
        btnPrev: ".next",
        vertical: false,
        mouseWheel: false,
        circular: true,
        visible: 5,
        speed: 800
    });
});