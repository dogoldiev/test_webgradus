<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!count($arResult['ITEMS'])) {
    return;
}

?>

<div class="block_green index">
    <div class="body">
        <div class="block_title left">
            <? if ($arParams['SHOW_TITLE'] == 'Y'):?>
            <div class="block_title end">
                <h2><?= Loc::getMessage('TITLE_CATALOG')?></h2>
            </div><!--/.block_title end -->
            <?endif?>
        </div><!--/.block_title -->
        <div id="jCarouselLite">
            <a class="next" href="javascript:void(0);"></a>
            <a class="prev" href="javascript:void(0);"></a>

            <div class="carousel">
                <ul>
                    <? foreach ($arResult['ITEMS'] as $item):?>
                        <li>
                            <div class="imgblock">
                                <a href="<?= $item['DETAIL_PAGE_URL']?>"><img src="<?= $item['PREVIEW_PICTURE']['SRC']?>" alt="" /></a>
                            </div>
                        </li>
                    <? endforeach;?>

                </ul>
            </div><!--/.carousel -->
        </div><!-- /jCarouselLite -->
    </div><!--/.body -->
    <div class="block_green_down"></div>
</div><!--/.block_green -->