<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$width  = PRODUCT_PIC_WIDTH ;
$height = PRODUCT_PIC_HEIGHT;

foreach ($arResult['ITEMS'] as &$item) {
    $file = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_EXACT);
    $item['PREVIEW_PICTURE']['SRC'] = $file['src'];
}
