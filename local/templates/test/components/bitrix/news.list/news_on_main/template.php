<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!count($arResult['ITEMS'])) {
     return;
}

?>
<div class="block_black">
    <div class="body">
    <div class="block_title left">
        <div class="block_title end">
            <h2><?= Loc::getMessage('TITLE')?></h2>
        </div>
    </div>
    <div class="block_news">
    <?$i = 1; foreach ($arResult['ITEMS'] as $item):?>
    <div class="block_show">
        <div class="post">
            <div class="small"><strong><?= $item['DISPLAY_ACTIVE_FROM']?></strong></div>
            <div class="title"><a href="<?= $item['DETAIL_PAGE_URL']?>"><?= $item['NAME']?></a></div>
            <div class="txt"><?= $item['PREVIEW_TEXT']?></div>
        </div>
    </div>
    <? if ($i == count($arResult['ITEMS'])) {
        continue;
    } ?>
    <div class="sep_big left"></div>
    <?$i++; endforeach;?>
</div>
</div>