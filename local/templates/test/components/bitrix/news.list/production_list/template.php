<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?foreach ($arResult['GROUP_BY_SECTIONS'] as $sectionId => $products):?>
    <div class="block_green nobg">
        <div class="body">
            <h2><?= $arResult['SECTIONS'][$sectionId]['NAME']?></h2>
            <p> <?= $arResult['SECTIONS'][$sectionId]['DESCRIPTION']?></p>
            <div class="nojCarouselLite">
                <div class="nocarousel">
                    <ul>
                        <?foreach ($products as $product):?>
                        <li>
                            <div class="imgblock">
                                <a href="<?= $product['DETAIL_PAGE_URL']?>">
                                    <img src="<?= $product['PREVIEW_PICTURE']['SRC']?>" title="<?= $product['NAME']?>"  alt="<?= $product['NAME']?>" />
                                </a>
                            </div>
                        </li>
                        <?endforeach;?>
                    </ul>
                </div><!--/.nocarousel -->
            </div><!-- /.nojCarouselLite -->
        </div><!--/.body -->
        <div class="block_gray_down"></div>
    </div><!--/.block_green -->
<?endforeach;?>