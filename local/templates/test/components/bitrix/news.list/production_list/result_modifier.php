<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$width = PRODUCT_PIC_WIDTH;
$height = PRODUCT_PIC_HEIGHT;
$arResult['GROUP_BY_SECTIONS'] = [];
foreach ($arResult['ITEMS'] as $key => &$item) {
    $file = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], ['width' => $width, 'height' => $height], BX_RESIZE_IMAGE_EXACT);
    $item['PREVIEW_PICTURE']['SRC'] = $file['src'];
    $arResult['GROUP_BY_SECTIONS'][$item['IBLOCK_SECTION_ID']][] = $item;
}
$sectionIds = array_keys($arResult['GROUP_BY_SECTIONS']);
$sections = \Bitrix\Iblock\SectionTable::getList([
    'filter' => [
        'ID' => $sectionIds
    ],
    'select' => ['NAME', 'DESCRIPTION', 'ID']
]);
while ($section = $sections->fetch()) {
    $arResult['SECTIONS'][$section['ID']] = $section;
}