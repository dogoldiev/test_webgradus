<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$itemSize = count($arResult);
$strReturn = '<div class="userbar">';

for ($index = 0; $index < $itemSize; $index++) {

    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $arrow = ($index > 0 ? '<i class="fa fa-angle-right"></i>' : '');

    if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1) {
        $strReturn .= "<a href='{$arResult[$index]["LINK"]}'>{$title}</a> <span><img src='/images/arrow.png' width='6' height='6' alt='' /></span>";
    } else {
        $strReturn .= $title;
    }
}
$strReturn .= '</div>';

return $strReturn;