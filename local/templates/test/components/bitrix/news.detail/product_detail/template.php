<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="block_green nobg">
    <div class="body">
        <div class="sideleft">
            <? if (!empty($arResult['DETAIL_PICTURE']['SRC'])):?>
                <img src="<?= $arResult['DETAIL_PICTURE']['SRC']?>"  alt="<?= $arResult['NAME']?>" />
            <?endif?>
        </div><!--/.sideleft -->
        <div class="dblock hidden">
            <h1><?=  $arResult['NAME']?></h1>
            <?= $arResult['DETAIL_TEXT']?>
        </div><!--/.dblock hidden -->
    </div><!--/.body -->
</div><!--/.block_green -->
