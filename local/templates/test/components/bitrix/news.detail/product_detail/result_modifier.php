<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$width = 254;
$height = 389;

if ($arResult['DETAIL_PICTURE']['ID']) {
    $file = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['ID'],
        ['width' => $width, 'height' => $height],
        BX_RESIZE_IMAGE_EXACT);

    $arResult['DETAIL_PICTURE']['SRC'] = $file['src'];
}