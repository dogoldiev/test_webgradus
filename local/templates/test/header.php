<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$asset = \Bitrix\Main\Page\Asset::getInstance();
$asset->addString('<meta charset="UTF-8">');

$asset->addCss('/style/index.css');

$asset->addString("<!--[if IE 7]><link rel=\"stylesheet\" type=\"text/css\" href='/style/ie7.css' media=\"screen\"><![endif]-->");

$asset->addJs('/js/jquery-1.6.1.min.js');

$asset->addJs('/js/jcarousellite_1.0.1.pack.js');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?php $APPLICATION->ShowTitle(); ?></title>
	<?php echo $APPLICATION->ShowHead(); ?>
</head>
<body>
<?php $APPLICATION->ShowPanel(); ?>
<div id="wrapper">
    <div id="header">
        <div class="dblock hidden">
            <a class="left" href="/">
                <img src="/images/logotype.png" width="278" height="116" alt="" title="Premiko - натуральный продукт из Венгрии" />
            </a>
            <div class="contacts">
                 <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/phone.php", Array(), Array("MODE" => "html")); ?>
                 <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/email.php", Array(), Array("MODE" => "html")); ?>
            </div>
<?php $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => array(),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "top",
		"MENU_CACHE_TYPE" => "Y"
	),
	false
); ?>
        </div>
        <?if (defined('INDEX_PAGE') && INDEX_PAGE):?>
        <div class="block_mainimg">
            <div><img src="/images/main_img.jpg" width="960" height="412" alt="" /></div>
            <div class="sticker">
                <div>
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/text_on_main.php", Array(), Array("MODE" => "html")); ?>
                </div>
            </div>
            <a class="sticker_midi" href="/production/"><span>Каталог продукции</span></a>
        </div>
        <?endif;?>
        <?if (!defined('INDEX_PAGE')):?>
            <?php $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "test" ,
                    []
            );?>
        <?endif;?>
    </div>
    <div id="container">
