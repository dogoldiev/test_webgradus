<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "head",
        "EDIT_TEMPLATE" => ""
    )
);?>

    <form class="registration" action="" enctype="application/x-www-form-urlencoded">
        <fieldset>
            <h2>Обратная связь</h2>

            <div><input type="text" onfocus="if(this.value=='Фамилия и имя')this.value='';" onblur="if(!this.value)this.value='Фамилия и имя';" value="Фамилия и имя" /></div>
            <div><input type="text" onfocus="if(this.value=='Электронная почта')this.value='';" onblur="if(!this.value)this.value='Электронная почта';" value="Электронная почта" /></div>
            <div><input type="text" onfocus="if(this.value=='Адрес')this.value='';" onblur="if(!this.value)this.value='Адрес';" value="Адрес" /></div>
            <div><input type="text" onfocus="if(this.value=='Телефон')this.value='';" onblur="if(!this.value)this.value='Телефон';" value="Телефон" /></div>
            <div><textarea cols="" rows="">Комментарии</textarea></div>
            <div><input class="btn_big_red left" type="submit" value="" /></div>
        </fieldset>
    </form>

    <?php
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "products_on_main",
    array(
        "COMPONENT_TEMPLATE" => "products_on_main",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "12",
        "NEWS_COUNT" => "5",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arFilterProductionOnMain",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d / m / Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "SHOW_TITLE" => "N"
    ),
    false
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>