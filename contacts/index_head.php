<div class="block_green visible">
    <div class="body visible" style="height: 150px;">
        <div class="map"><img src="/images/uploads/4.jpg" width="260" height="259" alt=""></div>
        <div class="dblock hidden" style="margin-left: 350px;">
            <h1>Продукция "Premiko"</h1>
            <div class="txt">На протяжении веков люди изобретали и улучшали самые разнообразные способы сохранения и
                консервирования продуктов. Стоит углубиться в тайны основных методов, благодаря которым наша
                кулинарная жизнь стала более качественной, интересной, а в первую очередь свежей.
            </div>
            <a class="sticker_midi" href="#" style="bottom: -80px;">
                <span>Каталог продукции</span>
            </a>
        </div><!--/.dblock hidden -->
    </div><!--/.body -->
    <div class="block_green_down"></div>
</div>