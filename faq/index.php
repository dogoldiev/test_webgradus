<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Вопрос-Ответ");
?>
    <div class="block_green nobg">
        <div class="body">
            <div class="artnews faq">
                <div class="post">
                    <div class="dblock hidden">
                        <div class="title">Раздел с контактной информацией. В данном разделе размещается форма обратной связи и карта Google со схемой проезда?</div>

                        <div class="txt">
                            <p><span class="answ">Ответ:</span> Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews faq">
                <div class="post">
                    <div class="dblock hidden">
                        <div class="title">Для того, чтобы попасть в раздел «Контакты», необходимо перейти по соответствующему пункту в меню?</div>

                        <div class="txt">
                            <p><span class="answ">Ответ:</span> Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews faq">
                <div class="post">
                    <div class="dblock hidden">
                        <div class="title">Раздел с контактной информацией. В данном разделе размещается форма обратной связи и карта Google со схемой проезда?</div>

                        <div class="txt">
                            <p><span class="answ">Ответ:</span> Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews faq">
                <div class="post">
                    <div class="dblock hidden">
                        <div class="title">Для того, чтобы попасть в раздел «Контакты», необходимо перейти по соответствующему пункту в меню?</div>

                        <div class="txt">
                            <p><span class="answ">Ответ:</span> Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews faq">
                <div class="post">
                    <div class="dblock hidden">
                        <div class="title">Раздел с контактной информацией. В данном разделе размещается форма обратной связи и карта Google со схемой проезда?</div>

                        <div class="txt">
                            <p><span class="answ">Ответ:</span> Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body nopad">
            <div class="pages">
                <a class="active" href="#">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">...</a>
                <a href="#">16</a>
            </div><!--/.pages -->
        </div><!--/.body -->
    </div><!--/.block_green -->

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>