<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>

    <div class="block_gray_down"></div>

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews">
                <div class="post">
                    <div class="imgpre">
                        <a href="article.html"><img src="/images/uploads/1.jpg" width="124" height="123" alt="" /></a>
                    </div><!--/.imgpre -->

                    <div class="dblock hidden">
                        <div class="title"><a href="article.html">Квашение зеленого горошка</a></div>

                        <div class="txt">
                            <p>Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->

                        <div>12 / 06 / 2012</div>
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews">
                <div class="post">
                    <div class="imgpre">
                        <a href="article.html"><img src="/images/uploads/2.jpg" width="124" height="123" alt="" /></a>
                    </div><!--/.imgpre -->

                    <div class="dblock hidden">
                        <div class="title"><a href="article.html">Овощные консервы венгерского производства</a></div>

                        <div class="txt">
                            <p>Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->

                        <div>12 / 06 / 2012</div>
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews">
                <div class="post">
                    <div class="imgpre">
                        <a href="article.html"><img src="/images/uploads/1.jpg" width="124" height="123" alt="" /></a>
                    </div><!--/.imgpre -->

                    <div class="dblock hidden">
                        <div class="title"><a href="article.html">Квашение зеленого горошка</a></div>

                        <div class="txt">
                            <p>Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->

                        <div>12 / 06 / 2012</div>
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews">
                <div class="post">
                    <div class="imgpre">
                        <a href="article.html"><img src="/images/uploads/2.jpg" width="124" height="123" alt="" /></a>
                    </div><!--/.imgpre -->

                    <div class="dblock hidden">
                        <div class="title"><a href="article.html">Плодоовощная консервация. Высокое качество.</a></div>

                        <div class="txt">
                            <p>Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->

                        <div>12 / 06 / 2012</div>
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->

        <div class="block_gray_down"></div>
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body">
            <div class="artnews">
                <div class="post">
                    <div class="imgpre">
                        <a href="article.html"><img src="/images/uploads/1.jpg" width="124" height="123" alt="" /></a>
                    </div><!--/.imgpre -->

                    <div class="dblock hidden">
                        <div class="title"><a href="article.html">Квашение зеленого горошка</a></div>

                        <div class="txt">
                            <p>Этот способ консервирования продуктов появился благодаря Николя Апперу в 1810 году. Суть его состоит в длительном приготовлении пищи в герметически закрытых емкостях, погруженных в кипящую воду. Консервирование происходит в закрытом пространстве без доступа воздуха.</p>
                        </div><!-- /.txt -->

                        <div>12 / 06 / 2012</div>
                    </div><!-- /.dblock hidden -->
                </div><!-- /.post -->
            </div><!-- /.artnews -->
        </div><!--/.body -->
    </div><!--/.block_green -->

    <div class="block_green nobg">
        <div class="body nopad">
            <div class="pages" style="margin-left: 160px;">
                <a class="active" href="#">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">...</a>
                <a href="#">16</a>
            </div><!--/.pages -->
        </div><!--/.body -->
    </div><!--/.block_green -->

    <?php
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "products_on_main",
    array(
        "COMPONENT_TEMPLATE" => "products_on_main",
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "12",
        "NEWS_COUNT" => "5",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arFilterProductionOnMain",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d / m / Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "STRICT_SECTION_CHECK" => "N",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "SHOW_TITLE" => "N"
    ),
    false
);?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>